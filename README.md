# Waze is a free, community-based traffic & navigation app - This is its unofficial Qt port #

## License ##
* GPLv2 - same as latest pulished Waze sources (v2.4)

## Important Notes ##
* Now that Google bought Waze, unless they hire me (damagedspline) to support this port, it is considered dead.
* Too many API changed between v2 & the latest v3 so some features stopped working.

 
## Notes ##
1. The base Waze code that is used is from the Android branch http://www.waze.com/wiki/index.php/Source_code
2. The following files were modified from the original waze sources (this list is mostly for me in case of codebase upgrade):
* *     # roadmap_gps.c
* *     # roadmap_dbread.c
* *     # roadmap_zlib.h
* *     # roadmap_general_settings.c
* *     # entry_ssd.h
* *     # entry_ssd.c
* *     # generic_search_dlg.c
* *     # Realtime.c
* *     # RealtimeAlerts.c
* *     # RealtimeUsers.c
* *     # generic_search.c
* *     # update_range.c
* *     # roadmap_address_ssd.c
* *     # roadmap_address_tc.c
* *     # roadmap_search.h
* *     # ssd_text.h
* *     # single_search_dlg.c
* *     # roadmap_search.c
* *     # roadmap_start.c
* *     # roadmap_res.c
* *     # roadmap_screen.c
* *     # roadmap_screen.h
* *     # roadmap_browser.c
* *     # roadmap_browser.h
* *     # string_parser.c
* *     # single_search_dlg.c
* *     # roadmap.h
* *     # roadmap_option.c
* *     # roadmap_urlschema.c
* *     # roadmap_nmea.c
* *     # roadmap_gpsd2.c
* *     # roadmap_prompts.c
* *     # RealtimeAlertsList.c
* *     # roadmap_skin.c
* *     # roadmap_lang.c
* *     # roadmap_factory.c
* *     # roadmap_splash.c
* *     # roadmap_io.c
* *     # editor_download.c
* *     # editor_sync.c
* *     # navigate_main.c
* *     # roadmap_browser.c
* *     # roadmap_download.c
* *     # roadmap_file.h
* *     # roadmap_login_ssd.c
* *     # roadmap_plugin.c
* *     # roadmap_sound.h
* *     # roadmap_tile_manager.c
* *     # LMap_Base.h
* *     # RealtimeExternalPoi.c
* *     # navigate_bar.c
* *     # roadmap_httpcopy.c
* *     # roadmap_net_mon.c
* *     # roadmap_recorder.c
* *     # roadmap_recorder_dlg.c
* *     # roadmap_screen.c
* *     # roadmap_welcome_wizard.c
* *     # ssd_button.c
* *     # ssd_container.c
* *     # ssd_keyboard_dialog.c
* *     # roadmap_file.h
* *     # roadmap_net.h
* *     # roadmap_serial.h
*   # The qt directory was based on the qt support from the roadmap project v1.2.1
** QtCanvas is used instead of agg/ogl
*   # QtMobility integration was added to allow interaction with the GPS, screensaver, etc..
*   # QtSql+SQLite replaces the previous SQLite tile storage & tts layer
*   # Missing the media files which I am not allowed to redistribute
*   # All of the media files (images & sounds) belong to Waze (c)

## How to build and run ##
*   # Open the waze.pro file using the QtCreator
*   # Build to the desired platform
*   # To Make a package and install on your device, do a Deploy after the build
*   # Since I am not allowed to distribute the media files, you will have to preform the following steps:
* *     # Get the latest Waze apk file you can find (Preferably later then Aug-11)
* *     # Extract it using an unzipper
* *     # In the extracted assets, copy Freemap to a new folder
* *     # Copy the FreemapHD content into the copied Freemap directory and overwrite all files
* *     # Execute the following line in the folder:
{{{
for i in `find ./skins -name '*.bin'`; do [ -f "`echo $i | sed 's/.bin$/.png/'`" ] || mv $i `echo $i | sed 's/.bin$/.png/'`; done
for i in `find ./sound -name '*.bin'`; do [ -f "`echo $i | sed 's/.bin$/.mp3/'`" ] || mv $i `echo $i | sed 's/.bin$/.mp3/'`; done
}}} 
    # From this project, copy the files under waze/data to the equivalent under the Freemap directory

## TODO list for the next versions (ordered by priority) ##

## v0.0.13 - Codename: Faruk - Next release ##
*   # Network fixes
*   # Main UI usability fixes

## v0.0.14 - Codename: Gdalia ##
*   # Rewrite the menus in QML - rethink main view UI - assisted by the community
*   # Add splash screen
*   # Some texts are cropped or missing
*   # Some images are missing
*   # Some action are hard to be done because of too small interface

## v0.1.0 - Codename: Haya ##
*   # Rewrite the map as a QML Location plugin - major performance improvements

# Previous Releases #
## 31-Mar-13 - v0.0.12 - codename: "Erlich" ##
*   # Don't force ports - this will allow using the Waze site servers
*   # Comment the map editing feature
*   # Don't force full screen when running on desktop
*   # Start using Waze v3 icons
*   # Improve the main menu looks
*   # Add a navigate button to the main menu
*   # Move the exit button from the main view to the main menu
*   # Reorganize the buttons in the main view
*   # Remake the ETA bar to be specific for protrait and landscape
*   # Make the next street name bigger

## 9-Jan-13 - v0.0.11 - codename: "Djako" ##
*   # Main view layout change
*   # Waze sounds no longer interrupts external media players

## 25-Oct-12 - v0.0.10 - codename: "Coco" ##
*   # Main view layout change
*   # When navigating, the "Navigate" button allow navigation control
*   # Minimize button added
*   # Browser control orientation fixed
*   # Multitouch on Harmattan fixed
*   # Portrait mode added
*   # Rotate map buttons were added (for platforms without multitouch)
*   # The number of active connections has been limited to ~16
*   # Fix the road signs text (road shields)
*   # Make the map fonts bolder

## 8-Oct-12 - v0.0.9-45 Beta - codename: "Charlie and a Half" ##
*   # Add AMOLED Optimized theme (contribution by ginggs)
*   # Fix routing navigation which sometimes didn't work
*   # Fix several network issues
*   # Small performance improvements
*   # Added Logging settings to General Settings
*   # Added Qt port release version to the about screen
*   # Remake most of the main view in QML
*   # Added portrait mode

## 17-May-12 - v0.0.9 Alpha - codename: "Chico" ##
*   # Added 'backlight stay lit only when plugged' option
*   # Fixed the freeze that occured when data was transmitted, so no more 'Close application...' dialog.
*   # Fixed network data compression (i consider working 90% of the time better than 0% )
*   # Fixed adding a favorite caused crash
*   # Fixed pinging a wazer caused crash
*   # Fixed some crashes that occured when starting a new search

## 2-May-12 - v0.0.8 Alpha - codename: "Batito" ##
*   # Implement secured connections
*   # Fixed empty mood list
*   # Fixed empty car list

## 19-Apr-12 - v0.0.7 Alpha - codename: "Azriel" => new naming convention based on Burekas movie characters ##
*   # Rewrite the network layer to be based on Qt instead of kernel sockets
*   # Rewrite the config layer to use QSettings
*   # Rewrite the path+file layer to use QFile & QDir
*   # Fix current traffic reports list dialog - reports not visible
*   # Support multiuser environments (such as Linux and Windows)
*   # Fix low battery warning still showing when charging